export default {
	
	target: 'static',

	server: {
		host: '0',
		ssr: false
	},

	head: {
		title: 'light0green',
		htmlAttrs: {
			lang: 'en'
		},
		meta: [
			{ charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'light0green (Yifan Pu) is a digital fashion freelancing artist.' }
		],
		link: [
			{ rel: "apple-touch-icon", sizes: "180x180", href: "/apple-touch-icon.png" },
			{ rel: "icon", type: "image/png", sizes: "32x32", href: "/favicon-32x32.png" },
			{ rel: "icon", type: "image/png", sizes: "16x16", href: "/favicon-16x16.png" }
		],
		script: [
			{ 'data-host': "https://microanalytics.io", 'data-dnt': "false", src: "https://microanalytics.io/js/script.js", id: "ZwSg9rf6GA", async: true, defer: true }
		]
	},

	css: [
		'~/assets/css/main.css'
	],

	plugins: [],

	components: true,

	buildModules: [],

	modules: [
		'@nuxt/content'
	],

	content: {
		markdown: {
			remarkPlugins: ['remark-unwrap-images']
		}
	},

	build: {}

}