---
draft: false
category: commissions
title: Maqu FW21 mini campaign
featured: false
collaborator: ''
client: ''
cover: "/bags-for-web.jpg"
wear:
  store: ''
  link: ''

---
FW21 mini 3d campaign for my local business - Maqu - a sustainable womenswear label based in Berlin and Lima

All pieces for this season are made of deadstock fabrics & yarns

\*

Alpaca neck warmers

\*

<video src="/neck-warmer-outdoor.mp4" autoplay loop muted playsinline></video>

<video src="/orange-neck-warmer.mp4" autoplay loop muted playsinline></video>

<video src="/grey-neck-warmer.mp4" autoplay loop muted playsinline></video>

\*

Neo Jacket and Luna Dress

\*

![](/jacket-web.jpg)

<video src="/jacket-walking.mp4" autoplay loop muted playsinline></video>

\*

Crossbags in three colorways

\-grey mesh with color blocking straps

\-black bag with grey straps

\-all black

\*

![](/bags-for-web.jpg)![](/details.png)details

<video src="/3-bags-slow.mp4" autoplay loop muted playsinline></video>

crossbags in an underwater scene highlighting its upcycled diving suit material

\*

\*

\*

Visit Maqu on [Instagram](https://www.instagram.com/by_maqu/) for a glance of the physical photoshoot

\*

\*

\*