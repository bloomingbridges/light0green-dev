---
title: The Pupil
category: commissions
client: Anyango Mpinga
cover: "/the-pupil/cover.jpg"
draft: false
featured: false
collaborator: ''
wear:
  store: ''
  link: ''

---
_The Pupil_ is a collection from the contemporary sustainable brand Anyango Mpinga.

The whole campaign was executed before production. All garments are digitally tailored first and then fit on real models' photos.

_“The reflection of a person is seen in the black pupil of the eye.” In other words, the “pupil of the eye” is a mirror of social reality.” -Abdu’l Baha_

The quote that inspired this collection is a reflection on Oneness and its importance in social justice and inclusion. Mpinga invited 9 dynamic women to share their insights on diversity and inclusion as reflected by our society today. Each garment presented in this collection has been created using innovative 3D digital tools by Yifan Pu. Creating this campaign virtually has given them the freedom to go borderless, reduce their carbon footprint and dress women living in different parts of the world from the comfort of their homes. This collection features bold prints that Mpinga designed; inspired by Swahili architecture on the Kenyan Coast, more specifically “Zidaka” niches, which are signature wall carvings traditionally used to display lamps and ornamental objects in Swahili Architecture.

<collection stub="/the-pupil/" format="jpg" items="12" speed="1000"></collection>

## Models

* _Aja Barber_
* _Monisola Omotoso_
* _Achieng Agutu_
* _Seiko Mbako_
* _Donna-Marie Mason_
* _Nyawira Mumenya_
* _Kalekye Mumo_
* _Tame McPherson_
* _Bucky Adejobi_

Read more about the inspiring women based in all over the world [here](https://anyangompinga.com/inspiration/).

## Product overview

![products-01](/the-pupil/products-01.jpg)

![products-02](/the-pupil/products-02.jpg)