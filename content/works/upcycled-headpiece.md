---
title: Upcycled Headpiece
category: collaborations
collaborator: All Amin aka Haramwithsugar
cover: "/upcycled-headpiece/cover.png"
wear:
  store: DRESSX
  link: https://dressx.com/products/puffer-suit-with-chain-deco
featured: false
draft: false
client: ''

---
I've gladly collaborated with upcycling artist *All Amin* (aka [haramwithsugar](https://www.instagram.com/haramwithsugar/)) to digitise this unique headpiece. 

[The physical piece](https://www.instagram.com/p/CGr413RgoRx/) was upcycled from a  pair of *Nike SB Air Force 2 Low Supreme Blue*.

To style the headpiece with a whole look, I then designed a puff blazer with chain decorations at the waist and a pair of matching boots.

<div>
	<video src="/upcycled-headpiece/upcycle_vid.mp4" autoplay loop muted playsinline></video>
	<video src="/upcycled-headpiece/catwalkx3.mp4" autoplay loop muted playsinline></video>
</div>

[Making-of](https://www.instagram.com/p/CAECAmvIc0o/)

![mv](/upcycled-headpiece/mv.jpg)

The 3d version of the headpiece is seen in music video [Fl00te by Zouj](https://www.youtube.com/watch?v=K7DsuFGNqC8).

![jacket](/upcycled-headpiece/jacket.gif)

The puff jacket is available for purchase on digital fashion retailer platform [DressX](https://dressx.com/products/puffer-suit-with-chain-deco)