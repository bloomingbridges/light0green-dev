---
title: Goose fashion
category: collaborations
collaborator: bloomingbridges
draft: false
featured: false
client: ''
cover: "/cover-4.jpg"
wear:
  store: ''
  link: ''

---
It all began from a doodle.

![](/goose1.jpg)

Then [bloomingbridges](https://twitter.com/bloomingbridges) modelled the geese and I dressed them in some #goosefashion.

![](/goose2.jpg)![](/goose3.jpg)![](/lily_full.jpg)

Now you can define your #goosefashion by downloading the goose model on [Sketchfab](https://sketchfab.com/3d-models/goosequin-cacac5a887604babb8e8b30a238f1fd3) for free and style them as you want:)