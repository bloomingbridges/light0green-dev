---
draft: false
category: personal
title: Nine-tailed Fox Hanfu
featured: false
collaborator: ''
client: ''
cover: "/cover-3.jpg"
wear:
  store: XR COUTURE
  link: https://xrcouture.com/collections/all-products/products/ninetale-hanfu

---
_Hanfu(汉服)_ is a collective name for traditional clothing of ethnic _Han_, who makes up roughly 92% of Chinese population nowadays.

The design of this look takes inspiration from Ming-Dynasty Hanfu styles and the Chinese mythical entity Nine-tailed fox spirit.

![](/top.jpg)

Crossed Hanfu collars form a _y_ shape at the front

![](/mamian.jpg)

This dress style called Ma-mian-qun(马面裙) got its popularity in Ming Dynasty. The feature is framed rectangles in the middle of the front and the back. They are tied together through a waist band with pleated side pieces.

The eyewear in this look is an interpretation of a modern fox spirit.

Fox spirits in ancient eastern myths reflect the demonization of enchanting females in a patriarchal context.

<video src="/glasses.mp4" autoplay loop muted playsinline></video>

This look had the honour to be featured on Vogue Poland as one of _the most beautiful digital costumes in the world_ [article link](https://www.vogue.pl/a/najpiekniejsze-cyfrowe-stroje-na-swiecie)

![](/two_next.gif)

digitally worn by [BLACKEARS](https://www.instagram.com/blackears_s/) (left) and [MeiMei](https://www.instagram.com/meimei_officialacc/ "MeiMei") 