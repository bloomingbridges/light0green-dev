---
title: Helsinki Fashion Week - The BLANCA dress
cover: "/hfw-the-blanca-dress/cover.jpg"
category: collaborations
collaborator: Anyango Mpinga
featured: true
draft: false
client: ''
wear:
  store: ''
  link: ''
---

![1](/hfw-the-blanca-dress/1.jpg)&nbsp;

![2](/hfw-the-blanca-dress/2.jpg)&nbsp;

![3](/hfw-the-blanca-dress/3.jpg)&nbsp;

The BLANCA dress is designed by *Anyango Mpinga* for Helsinki Fashion Week 2020. 

The dress was by then digital-only.



Digital avatar by *Paola Pinna*

Digital clothing by me