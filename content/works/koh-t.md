---
title: Nostalgic Double Wrapped Coat
cover: "/koh-t/cover.jpg"
category: collaborations
collaborator: KoH T
draft: false
featured: false
client: ''
wear:
  store: ''
  link: ''

---
![0](/koh-t/0.jpg)
![1](/koh-t/1.jpg)
![2](/koh-t/2.gif)

Digital presentation of *Nostalgic Double Wrapped Coat* 

by Japanese brand *KoH T* for *Helsinki Fashion Week* 2020

*"The wide trousers have a rough lace woven texture and feel like homemade. The bow tie dress is 100% made of linen. The overcoat is a two-layer combination coat. The layer under is made of raffia, giving a feeling of nature. The layer over is made  with transparent silk lame organza."*