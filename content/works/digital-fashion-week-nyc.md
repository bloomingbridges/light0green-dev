---
draft: false
category: personal
title: Reflection - Digital Fashion Week NYC
featured: false
collaborator: ''
client: ''
cover: "/foam-nft0067.png"
wear:
  store: ''
  link: ''

---
_Reflection_ is a short fashion film that was premiered on Digital Fashion Week NYC in January 2022.

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/743500952?h=8e90b62f02&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Reflection"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

Concept WIP

![](/conceptwip1.png)

![](/conceptwip2.jpg)

![](/conceptwip3.png)