---
draft: false
category: commissions
title: JAKE LIU SS2021
featured: true
collaborator: ''
client: JAKE LIU
cover: "/cover.jpg"
wear:
  store: DressX
  link: https://dressx.com/search?q=jake+liu

---
In JAKE LIU SS21 I was responsible for 3d modelling/assembling of accessories, avatar rigging and posing.

Digital garments by [_Aya Balboa Harada_](https://www.instagram.com/rittaifuku/)

Graphic design by [_Elliot Frydenberg_](https://www.instagram.com/elliot.fry/)

![](/boots2.jpg)
![](/hat.jpg)
![](/boots.jpg)