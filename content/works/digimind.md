---
draft: false
category: exhibitions
title: DigiMind
featured: false
collaborator: ''
client: ''
cover: "/digimind_cover.jpg"
wear:
  store: ''
  link: ''

---
DigiMind is part of [Digital Artist Recidency](https://www.instagram.com/digitalartistresidency/)'s online residency during June-September in 2020.

DigiMind is a group exhibition exploring themes of mysticism, technology and spirituality through CGI and virtual environments. The Oracle is activated by visitors feeding it questions and in turn it will offer a visual response created by one of the artists included in the exhibition.

[Revisit the exhibition. Let the Oracle answer your questions.](https://digitalartistresidency.org/digimind/)

Artists:

* [_Paola Pinna_](https://www.instagram.com/paola_pinna_artist/)
* [_Aylin Delemen_](https://www.instagram.com/aylindelemen/)
* [_Sophie Rogers_](https://www.instagram.com/sophieirogers/)
* [_Camilla Roriz_](https://www.instagram.com/camilaroriz/)
* [_Kerrie IRL_](https://www.instagram.com/kerrie.irl/)
* [_Ashleigh Sanderson_](https://www.instagram.com/byashsand/)
* [_Yifan Pu_]()

Selection of my contribution↓

![](/bird-cage1.png)&nbsp;
![](/night-yoga-graded.jpg)&nbsp;
![](/leaving-woman.jpg)&nbsp;
![](/kid.jpg)&nbsp;