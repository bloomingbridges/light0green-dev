---
draft: false
category: commissions
title: WETWARE
featured: false
collaborator: ''
client: Together We Dance Alone
cover: "/cover-2.jpg"
wear:
  store: ''
  link: ''

---
WETWARE is an interactive shirt by clubwear label Together We Dance Alone (TWDA).

The shirt features an Augmented Reality (AR) marker hidden in the TWDA double-logo. When you track it with your back camera via their [account](https://www.instagram.com/togetherwedancealone/), the logo comes alive.

![](/gang.jpg)![](/glitch.jpg)

<video src="/motion.mp4" autoplay loop muted playsinline></video>

More about this project on [VOGUE.de](https://www.vogue.de/mode/artikel/dixon-clubwear) (in German)