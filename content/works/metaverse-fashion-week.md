---
draft: false
category: commissions
title: Oyster Spell for Metaverse Fashion Week
featured: true
collaborator: ''
client: XR Couture
cover: "/oyster_cover.jpg"
wear:
  store: XR Couture
  link: https://app.xrcouture.com/nftDetail?id=21

---
<video src="/square360.mp4" autoplay loop muted playsinline></video>

<video src="/bts.mp4" autoplay loop muted playsinline></video>

![](/bandicam-2022-03-27-14-23-06-436.jpg)

![](/bandicam-2022-03-26-18-58-32-328.jpg)

![](/bandicam-2022-03-24-18-07-53-808.jpg)

![](/aaron.jpg)

last pic cr. twitter@aaronhuey

Users wearing Oyster Spell having fun in Decentraland's Metaverse Fashion Week in March 2022