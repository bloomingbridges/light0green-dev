---
title: SIMULACRUM
cover: "/simulacrum/cover.jpg"
category: collaborations
collaborator: Paulina Münzing
featured: true
draft: false
client: ''
wear:
  store: ''
  link: ''

---
<collection stub="/simulacrum/collection" items="8"></collection>
<video src="/simulacrum/loop.mp4" autoplay loop muted playsinline></video>

<div>
<div class="gallery" style="flex-wrap: wrap;">
<video class="gallery-item" src="/simulacrum/fisheye1.mp4" autoplay loop muted playsinline></video>
<video class="gallery-item" src="/simulacrum/fisheye2.mp4" autoplay loop muted playsinline></video>
<video class="gallery-item" src="/simulacrum/pulled_animation.mp4" autoplay loop muted playsinline></video>
</div>
<div class="gallery">
<video class="gallery-item" src="/simulacrum/text_1.mp4" autoplay loop muted playsinline></video>
<video class="gallery-item" src="/simulacrum/text_2.mp4" autoplay loop muted playsinline></video>
<video class="gallery-item" src="/simulacrum/text_3.mp4" autoplay loop muted playsinline></video>
</div>
</div>

SIMULACRUM is a phygital collection inspired by the Internet culture. It illustrates how the designer feels facing the flood of digital images. Designed and physically crafted by fashion postgraduate _Paulina Münzing_, SIMULACRUM is re-imagined in digital by me using glitch and physically impossible motions.

_"Inspired by the overpopulation of digital images we manipulate by copying and sharing, the work explores how the hyperlinked network structure invades our identities and questions our emotional relationship with the internet._"