---
draft: false
category: exhibitions
title: Night Embassy
featured: false
collaborator: ''
client: ''
cover: "/nightembassy_cover.jpg"
wear:
  store: ''
  link: ''

---
NIGHT EMBASSY is a 2-week residency sponsored by Jägermeister exploring the future of Berlin’s nightlife with curated exhibition, panels, performances and club nights.

I joined as part of collective [Digi-Gxl](https://www.instagram.com/digi.gxl/?hl=en) in collaboration with music collective [CO:QUO](https://www.instagram.com/coquo.co/)

OFFICIAL SITE [https://night-embassy.com/ambassador/coquo-x-digigxl](https://night-embassy.com/ambassador/coquo-x-digigxl "https://night-embassy.com/ambassador/coquo-x-digigxl")

> _"New technologies means new horizons on the dancefloor: CO:QUO x Digi-Gxl are breaking boundaries and exploring what it means to be human, in increasingly virtual environments with AI, VR and a focus on quality music and inclusivity."_

![](/entrance2.jpg)![](/dancing.jpg)![](/texturing.jpg)![](/videoinstallation.jpg)

video installation of dancing avatars with live performance

(people in the picture above, top to bottom - [_Navild Acosta_]()_,_ [_Rude Vianna_](https://www.instagram.com/cocota_rud/)_)_

You can check out our dancing avatar video [here](https://vimeo.com/360272531).

![](/3d.jpg)

3d printed light-reactive necklace (3d modelled by [_Harriet Davey_](https://www.instagram.com/harriet.blend/), modified by me, 3d printed by [_Caroline Barrueco_](https://www.instagram.com/ccaarroollliinnee/), in the picture - [_Miriam Woodburn_](https://www.instagram.com/miriamwoodburn/)

<video src="/turtlecam.mp4" autoplay loop playsinline></video>

video clip from my VJ set for [_Alison Swing_](https://soundcloud.com/alisonswing)

Team:

collaboration between _Digi-Gxl Berlin_ and _CO:QUO_

motion capture tech support [_Mimic Productions_](https://www.instagram.com/mimic_productions/)

dancing avatars video [_Maria Gudjohnsen_]()

photography by [_Camille Blake_](https://www.instagram.com/camille_blake/)