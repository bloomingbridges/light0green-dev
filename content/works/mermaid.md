---
title: Virtual Couture
cover: "/mermaid/cover.jpg"
category: personal
draft: false
featured: false
collaborator: ''
client: ''
wear:
  store: XR COUTURE
  link: https://xrcouture.com/collections/featured/products/mythical-mermaid-gown-yifan-pu

---
After reading [_The Fabricant_](https://www.instagram.com/the_fab_ric_ant/)'s article [_FROM HAUTE COUTURE TO THOUGHT COUTURE_](https://www.thefabricant.com/blog/2019/10/3/from-haute-couture-to-thought-couture#:\~:text=Couture.,to%20mean%20so%20much%20more.), I've been thinking about how the term _virtual couture_ can be defined, presented and acknowledged.

The ongoing mermaid series is my response to the questions above.

![](/dress1.gif) 

virtual fitting on a _Jennie Kim_ photo as my entry for the challenge #iDressdigital initiated by [XR.COUTURE](https://xrcouture.com/) and [_Michelle Dominique Cazar_](https://www.instagram.com/mcazar.design/) to promote digital fashion.

![](/dress1makeup.jpg)

digital makeup behind-the-scene

![](/miranda.jpg)

virtual fitting on model _Miranda Marquez_ as part of an [editorial for KALTBLUT magazine](https://www.kaltblut-magazine.com/the-emperors-dress/)

![](/worn-wide.jpg)

digitally worn by _Obianibeli Esu_

![](/2.jpg)

layered translucent cape, gradient velvet tail with decorative fur

![](/dandelion.jpg)

conceptual dandelion dress, original earrings

![](/sky.jpg)

Imaginated scene: a mermaid's red carpet moment