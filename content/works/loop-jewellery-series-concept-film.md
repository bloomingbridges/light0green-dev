---
draft: false
category: commissions
title: "'LOOP'Jewellery Series[concept film]"
featured: false
collaborator: ''
client: Zhuo Hua
cover: "/frame3.jpg"
wear:
  store: ''
  link: ''

---
The 1-minute concept film showcases jewellery designer Zhuo Hua's series LOOP.

 ![](/frame2.jpg)

This exoskeleton-inspired series consists of five pieces - the Mobius-bracelet, asymmetric eyewear, the key-ring, a chest piece and a shoulder piece with a memory card.

 ![](/frame1.jpeg)

Together Zhuo Hua and I envisioned a post-human world where communication will be done with the aid of cyberwear and achieves instant understanding.
 
 ![](/ring-still.jpg)
 
<video src="/zhubaocredit.mp4" autoplay loop muted playsinline></video>