---
draft: false
category: collaborations
title: Biomimicry Dress with AUROBOROS
featured: false
collaborator: ''
client: ''
cover: "/closeup-ps.jpg"
wear:
  store: ''
  link: ''

---
![](/front_side_mika_kailes.jpg)

photo by _Mika Kailes_

_Biomimicry_ is originally a couture collection by fashion/tech house _AUROBOROS_.

This green jade gown was a piece from the collection that exists both in digital and in physical.

The physical version was worn by AI robot Ai-Da at the Victoria and Albert Museum for London Design Festival 2021.

![](/exhibit-back.jpg)photo from [AUROBOROS](https://www.instagram.com/auro.boros/)

I had the pleasure to aid this production at an early stage with 3d modelling and visualisation.

<video src="/360.mp4" autoplay loop muted playsinline></video>

![](/backps.jpg)![](/frontps.jpg)

The look features a Victorian corset underneath, supporting a time glass shape, and biomimetic tentacles surrounding the body plus an elegant long train.

The physical piece is decorated with growing crystals, making the dress alive.

material experiment:

![](/experiment.jpg)