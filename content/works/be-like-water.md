---
title: Be Like Water
cover: "/be-like-water/cover.jpg"
category: collaborations
collaborator: purplemoon.3d
draft: false
featured: false
client: ''
wear:
  store: ''
  link: ''

---
_Be Like Water_ is a mini project made possible by collaborator [@purplemoon.3d](https://www.instagram.com/purplemoon.3d/) (previously @i.am.3d) She was generously giving away the 3d file of her _outfit.002_.

I retextured and styled _outfit.002_ with my original _fluid texture boots_.

![belikewater](/be-like-water/belikewater.gif)The calligraphy _上善若水_ is a Chinese idiom originated from the Daoist classic _Dao Te Ching_. It means top virtue is to be like water, as water takes no form and benefits everything while staying low and running deep.

<div>
<video src="/be-like-water/ghostwalk.mp4" autoplay loop muted playsinline></video>
</div>