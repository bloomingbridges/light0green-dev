---
draft: false
category: collaborations
title: The Back that Carried the Chosen One
featured: false
collaborator: MASAMARA
client: ''
cover: "/promo1_fixed_sameprint.png"
wear:
  store: ''
  link: ''

---
MUGONGO WAHETSE INTORE  
(Rwandan for The Back that Carried the Chosen One)

is a collaboration under the art direction of Nyambo MasaMara as a special teaser for his MASAMARA collection, as well as a love letter for his mother and grandfather who guided him into fashion, textile and everything creative.

![](/masamara-gown.jpg)![](/giants.jpg)

<div> <video src="/longer.mp4" autoplay loop muted playsinline></video> </div>

[Making-of](https://www.instagram.com/p/Ce6sKnFKyP7/)