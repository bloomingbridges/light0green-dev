---
draft: false
category: personal
title: Capybara back ride
featured: false
collaborator: ''
client: ''
cover: "/capybara-back-ride-on-valslooks.jpg"
wear:
  store: zero10
  link: https://zero10.page.link/xVYP

---
Capybara Back Ride is my first AR outfit made possible with support from zero10 app.

<video src="/capybara_back_ride.mp4" autoplay loop muted playsinline></video>

I noticed a capybara hype around me. Their laid-back attitude combined with the cute figure is said to be brilliant for a mood boost. I decided to embrace that in my design and AR is perfect for trying something a bit unrealistic. I went with a huge stuffed capybara on the back, a unisex overall underneath with a loose fit.

![](/oufit2.png)![](/top_2.png)

It was designed with different styling options in mind.

<video src="/valsvideo.mp4" autoplay loop muted playsinline></video>

<video src="/zero10.mp4" autoplay loop muted playsinline></video>


![](/img_9314.JPG)