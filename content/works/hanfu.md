---
title: Zero-waste Hanfu inspired loungewear
cover: "/cover-1.jpg"
category: personal
wear:
  store: DRESSX
  link: https://dressx.com/products/loungewear
draft: false
featured: false
collaborator: ''
client: ''
---

Zero-waste refers to a sustainable pattern making method where no fabric piece is wasted.

Hanfu is a collective name for traditional clothing of ethnic Han, who makes up roughly 92% of Chinese population.

Since square-y patterns are already a feature of Hanfu, I decided to practice zero-waste methodology with a Hanfu-inspired look.

![](/patterns.jpg)

![](/zerowaste.jpg)

> Patterns of each chosen fabric fit into a rectangle where no waste is produced.</span>

![](/back.jpg)

![](/loungewear1.jpg)

> Chiffon texture by textile company SUNGMIN ENTERPRISE CO.,LTD.