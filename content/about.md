---
title: About

---
## Bio

Hi I'm _Yifan Pu_ (sometimes as _light0green_). I'm a Chinese virtual fashion designer and artist currently based in Berlin.

I taught myself CLO3d with a previous knowledge of 3d modelling and a constant passion for fashion. I find virtual fashion a perfect tool to express pure creation and a second self-identity freed from physical limitations, mass production and consumption.

The username _light0green_ is a combination of my favourite colour light green and the character _Yagami Light_ (pronounced Rai-to) from anime _Death Note_.

[Download CV](https://drive.google.com/file/d/1cqO5_wffSBqj2Wqp2VAz4OMWejXk78Bl/view?usp=share_link)